import visualizematchesPlayedPerYear from "./visualize/visualizematchesPlayedPerYear.js";
import visualizematchesWonPerYear from "./visualize/visualizematchesWonPerYear.js";
import visualizeextraRunsIn2016 from "./visualize/visualizeextraRunsIn2016.js";
import visualizeeconomicalBowlersEachYear from "./visualize/visualizeeconomicalBowlersEachYear.js";
import visualizeteamWinsperVenue from "./visualize/visualizeteamWinsperVenue.js";
import visualizetossWonAndMatchWon from "./visualize/visualizetossWonAndMatchWon.js";
import visualizeplayerOfTheMatch from "./visualize/visualizeplayerOfTheMatch.js";
import visualizestrikeRateOfBatsmen from "./visualize/visualizestrikeRateOfBatsmen.js";
import visualizeplayerDismissed from "./visualize/visualizeplayerDismissed.js";
import visualizesuperOverEconomy from "./visualize/visualizesuperOverEconomy.js";
import visualizeFetchedEconomicalEachYear from "./visualize/visualizeFetchedEconomicalEachYear.js";

let FILENAME = [
    "matchesPlayedPerYear",
    "matchesWonPerYear",
    "extraRunsIn2016",
    "economicalBowlersEachYear",
    "teamWinsperVenue",
    "tossWonAndMatchWon",
    "playerOfTheMatch",
    "strikeRateOfBatsmen",
    "playerDismissed",
    "superOverEconomy",
];

const fetchAndVisualizeEconomicalBowlers = () => {
    let selectButton = document.getElementById("select-button");
    let yearSelected = document.getElementById("select-container");
    selectButton.onclick = () => {
        fetchEconomical(yearSelected.value);
    };

    const fetchEconomical = (year) => {
        let url = "https://ipl-economy-data-server.herokuapp.com/economy/";
        url += year;
        fetch(url)
            .then((r) => r.json())
            .then((data) => visualizeFetchedEconomicalEachYear(data, year));
    };
    fetchEconomical(yearSelected.value);
};

const fetchAndVisualizeData = () => {
    for (let index = 0; index < FILENAME.length; index++) {
        fetch(`./output/${FILENAME[index]}.json`)
            .then((result) => result.json())
            .then((data) => eval(`visualize${FILENAME[index]}`)(data));
    }
};

fetchAndVisualizeEconomicalBowlers();
fetchAndVisualizeData();
