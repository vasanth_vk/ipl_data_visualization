
const visualizematchesPlayedPerYear = (matchesPlayedPerYear) => {
    const seriesData = Object.entries(matchesPlayedPerYear);

    Highcharts.chart("matches-played-per-year", {
        chart: {
            type: "column",
        },
        title: {
            text: "Matches Played Per Year",
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
        },
        yAxis: {
            min: 0,
            title: {
                text: "Matches",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Matches: <b>{point.y:.0f}</b>",
        },
        series: [
            {
                name: "Years",
                data: seriesData,
            },
        ],
    });
};

export default visualizematchesPlayedPerYear;