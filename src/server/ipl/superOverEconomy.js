const superOverEconomy = (deliveries) => {
    let bowlersData = {};
    let result = {}
    for (let delivery of deliveries) {
        let { is_super_over, bowler, total_runs } = delivery;

        if (is_super_over != 0) {
            if (bowlersData[bowler] == undefined) {
                bowlersData[bowler] = [1, total_runs];
            } else {
                bowlersData[bowler][0] += 1;
                bowlersData[bowler][1] += total_runs;
            }
        }
    }
    for (let bowler in bowlersData) {
        let oversBowled = bowlersData[bowler][0] / 6;
        let runsConceded = bowlersData[bowler][1];
        result[bowler] = Math.floor((runsConceded / oversBowled) * 100) / 100;
    }

    return Object.fromEntries(Object.entries(result).sort((a, b) => a[1] - b[1]));
};

module.exports = superOverEconomy;
