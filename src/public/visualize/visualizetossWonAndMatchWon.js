const visualizetossWonAndMatchWon = (tossWonAndMatchWon) => {
    let data = Object.entries(tossWonAndMatchWon);

    Highcharts.chart("toss-wins", {
        chart: {
            type: "column",
        },
        title: {
            text: "number of times each team won the toss and also won the match",
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
        },
        yAxis: {
            min: 0,
            title: {
                text: "Wins",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Wins: <b>{point.y:.0f}</b>",
        },
        series: [
            {
                name: "Teams",
                data: data,
            },
        ],
    });
};

export default visualizetossWonAndMatchWon;
