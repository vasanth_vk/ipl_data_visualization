

const visualizeplayerDismissed = (playerDismissed) => {
    let batsman = Object.keys(playerDismissed);
    let bowlers = Object.entries(playerDismissed[batsman]);

    Highcharts.chart("player-dismissed", {
        chart: {
            type: "column",
        },
        title: {
            text: `The highest number of times ${batsman} has been dismissed by another player`,
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: 0,
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: "Dismissals",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Dismissals: <b>{point.y:.0f}</b>",
        },
        series: [
            {
                name: "dismissals",
                data: bowlers,
            },
        ],
    });
};

export default visualizeplayerDismissed;