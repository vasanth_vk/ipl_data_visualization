const tossWonAndMatchWon = (matches) => {
    let result = {}
    for (let match of matches) {
        let { toss_winner, winner } = match;
        if (toss_winner === winner) {
            if (result[winner] === undefined) {
                result[winner] = 1;
            }
            else {
                result[winner] += 1;
            }
        }
    }
    return result;
}


module.exports = tossWonAndMatchWon;