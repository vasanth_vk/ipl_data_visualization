const playerOfTheMatch = (matches) => {
    let allSeasonsPlayerOfMatch = {};
    let result = {};
    for (let match of matches) {
        let { season, player_of_match } = match;
        if (allSeasonsPlayerOfMatch[season] == undefined) {
            allSeasonsPlayerOfMatch[season] = {};
            allSeasonsPlayerOfMatch[season][player_of_match] = 1;
        } else {
            if (allSeasonsPlayerOfMatch[season][player_of_match] == undefined) {
                allSeasonsPlayerOfMatch[season][player_of_match] = 1;
            } else {
                allSeasonsPlayerOfMatch[season][player_of_match] += 1;
            }
        }
    }
    for (let season in allSeasonsPlayerOfMatch) {
        let player = Object.entries(allSeasonsPlayerOfMatch[season]).sort((a, b) => a[1] - b[1]).pop()
        result[`${season} - ${player[0]}`] = player[1]
    }
    return result;
};
module.exports = playerOfTheMatch;
