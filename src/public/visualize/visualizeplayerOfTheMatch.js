
const visualizeplayerOfTheMatch = (playerOftheMatch) => {
    let data = Object.entries(playerOftheMatch);

    Highcharts.chart("player-of-the-match", {
        chart: {
            type: "column",
        },
        title: {
            text:
                "Player who has won the highest number of Player of the Match awards for each season",
        },
        subtitle: {
            text:
                "Source: <a href='https://www.kaggle.com/nowke9/ipldata/data'>IPL Dataset</a>",
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: -45,
                style: {
                    fontSize: "13px",
                    fontFamily: "Verdana, sans-serif",
                },
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: "Player of the match",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "<b>Player of the match awards won - {point.y:0.0f}</b>",
        },
        series: [
            {
                name: "player of thematch",
                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: "#000",
                    align: "center",
                    format: "{point.y:.0f}",
                    y: 0,
                    style: {
                        fontSize: "13px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            },
        ],
    });
};

export default visualizeplayerOfTheMatch;