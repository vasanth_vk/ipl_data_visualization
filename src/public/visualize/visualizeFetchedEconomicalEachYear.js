const visualizeFetchedEconomicalEachYear = (economicalBowlersOf2015, year) => {
    const data = Object.entries(economicalBowlersOf2015);

    Highcharts.chart("economical-year", {
        chart: {
            type: "column",
        },
        title: {
            text: `The economical bowlers in ${year}`,
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: 0,
                style: {
                    fontSize: "13px",
                    fontFamily: "Verdana, sans-serif",
                },
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: "Economy",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Economy - <b>{point.y:.2f}</b>",
        },
        series: [
            {
                name: "Economy",
                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: "#000",
                    align: "center",
                    format: "{point.y:.2f}",
                    y: 0,
                    style: {
                        fontSize: "12px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            },
        ],
    });
};

export default visualizeFetchedEconomicalEachYear;