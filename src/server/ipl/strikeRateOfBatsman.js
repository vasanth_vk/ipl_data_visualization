const batsmanToFindSR = 'V Kohli'
const strikeRateOfBatsman = (matches, deliveries) => {
    let batsmanData = {};
    let result = {};
    let yearAndId = {}
    for (let match of matches) {
        let { season, id } = match;
        if (yearAndId[season] == undefined) {
            yearAndId[season] = [id]
        }
        else {
            yearAndId[season].push(id)
        }
    }
    for (let year in yearAndId) {
        for (let delivery of deliveries) {
            let { match_id, batsman, batsman_runs } = delivery;

            if (yearAndId[year].includes(match_id) && batsman == batsmanToFindSR) {
                if (batsmanData[year] == undefined) {
                    batsmanData[year] = [1, batsman_runs]
                }
                else {
                    batsmanData[year][0] += 1;
                    batsmanData[year][1] += batsman_runs;
                }
            }
        }
    }
    for (let year in batsmanData) {
        let ballFaced = batsmanData[year][0];
        let runsScored = batsmanData[year][1];
        result[year] = Math.round(runsScored * 100 / ballFaced * 100) / 100;
    }

    return { [batsmanToFindSR]: result }
}


module.exports = strikeRateOfBatsman;