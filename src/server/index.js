const fs = require("fs");
const path = require("path");
const csv = require("csvtojson");
const matchesPlayedPerYear = require("./ipl/matchesPlayedPerYear");
const matchesWonPerYear = require("./ipl/matchesWonPerYear");
const extras = require("./ipl/extras");
const economical = require("./ipl/topEconomicalBowler");
const teamWinbyVenue = require("./ipl/teamWinbyVenue");
const tossWonAndMatchWon = require("./ipl/tossWonAndMatchWon");
const playerOfTheMatch = require("./ipl/playerOfTheMatch");
const strikeRateOfBatsman = require("./ipl/strikeRateOfBatsman");
const playerDismissed = require("./ipl/playerDismissed");
const superOverEconomy = require("./ipl/superOverEconomy");
const MATCHES_FILE_PATH = path.join(__dirname, "../csv_data/matches.csv");
const JSON_OUTPUT_FILE_PATH = path.join(__dirname, "../public/output/");
const DELIVERIES_FILE_PATH = path.join(__dirname, "../csv_data/deliveries.csv");

function main() {
  csv({ checkType: true })
    .fromFile(MATCHES_FILE_PATH)
    .then((matches) => {
      csv({ checkType: true })
        .fromFile(DELIVERIES_FILE_PATH)
        .then((deliveries) => {
          //MATCHES PLAYED PER YEAR
          let result1 = matchesPlayedPerYear(matches);

          //number of matches won by each team over all the years of IPL
          let result2 = matchesWonPerYear(matches);

          //For the year 2016, plot the extra runs conceded by each team.
          let result3 = extras(matches, deliveries);

          //Top economical bowlers of 2015
          let result4 = economical(matches, deliveries);

          //matches won by each team per venue
          let result5 = teamWinbyVenue(matches);

          //the number of times each team won the toss and also won the match
          let result6 = tossWonAndMatchWon(matches);

          //Find a player who has won the highest number of *Player of the Match* awards for each season
          let result7 = playerOfTheMatch(matches);

          // Find the strike rate of a batsman for each season
          let result8 = strikeRateOfBatsman(matches, deliveries);

          //Find the highest number of times one player has been dismissed by another player
          let result9 = playerDismissed(deliveries);

          //Find the bowler with the best economy in super overs
          let result10 = superOverEconomy(deliveries);

          saveJson(
            result1,
            result2,
            result3,
            result4,
            result5,
            result6,
            result7,
            result8,
            result9,
            result10
          );
        });
    });
}

function saveJson(
  result1,
  result2,
  result3,
  result4,
  result5,
  result6,
  result7,
  result8,
  result9,
  result10
) {
  let fileName = [
    "matchesPlayedPerYear",
    "matchesWonPerYear",
    "extraRunsIn2016",
    "economicalBowlersEachYear",
    "teamWinsperVenue",
    "tossWonAndMatchWon",
    "playerOfTheMatch",
    "strikeRateOfBatsmen",
    "playerDismissed",
    "superOverEconomy",
  ];

  for (let index = 0; index < arguments.length; index++) {
    let jsonData = arguments[index];

    const jsonString = JSON.stringify(jsonData, null, 4);
    fs.writeFile(
      `${JSON_OUTPUT_FILE_PATH}${fileName[index]}.json`,
      jsonString,
      "utf8",
      (err) => {
        if (err) {
          console.error(err);
        }
      }
    );
  }
}

main();
