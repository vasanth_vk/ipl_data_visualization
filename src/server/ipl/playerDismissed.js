const batsman = 'RG Sharma'
const playerDismissed = (deliveries) => {

    let result = {}

    for (let delivery of deliveries) {

        let { bowler, player_dismissed } = delivery;

        if (player_dismissed == batsman) {

            if (result[bowler] == undefined) {
                result[bowler] = 1;
            }

            else {
                result[bowler] += 1;
            }

        }
    }

    return { [batsman]: Object.fromEntries(Object.entries(result).sort((a, b) => b[1] - a[1]).slice(0, 15)) };
}
module.exports = playerDismissed;