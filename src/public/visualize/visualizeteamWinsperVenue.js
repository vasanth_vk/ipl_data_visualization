const visualizeteamWinsperVenue = (teamWinsperVenue) => {
    let series = [];
    let stadiums = [];
    let teams = [];

    for (let venue in teamWinsperVenue) {
        stadiums.push(venue);
        for (let team in teamWinsperVenue[venue]) {
            if (!teams.includes(team) && team != "") teams.push(team);
        }
    }
    for (let team of teams) {
        // console.log(team)
        let obj = {};
        obj["name"] = team;
        obj["data"] = [];
        for (let stadium of stadiums) {
            if (teamWinsperVenue[stadium].hasOwnProperty(team)) {
                obj["data"].push(teamWinsperVenue[stadium][team]);
            } else {
                obj["data"].push(0);
            }
        }
        series.push(obj);
    }

    Highcharts.chart("venue-wins", {
        chart: {
            type: "bar",
        },
        title: {
            text: "Matches won by each team per venue",
        },
        xAxis: {
            categories: stadiums,
        },
        yAxis: {
            min: 0,
            title: {
                text: "Total matches won",
            },
        },
        legend: {
            reversed: true,
        },
        plotOptions: {
            series: {
                stacking: "normal",
            },
        },
        series: series,
    });
};

export default visualizeteamWinsperVenue;
