const visualizestrikeRateOfBatsmen = (strikeRateOfBatsman) => {
    let batsman = Object.keys(strikeRateOfBatsman);
    let strikeRate = Object.entries(strikeRateOfBatsman[batsman]);

    Highcharts.chart("strike-rate", {
        chart: {
            type: "column",
        },
        title: {
            text: `The strike rate of ${batsman} for each season`,
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: 0,
                style: {
                    fontSize: "13px",
                    fontFamily: "Verdana, sans-serif",
                },
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: "Strike rate",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Strike Rate - <b>{point.y:.1f}</b>",
        },
        series: [
            {
                name: "strike rate",
                data: strikeRate,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: "#000",
                    align: "right",
                    format: "{point.y:.1f}", // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: "13px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            },
        ],
    });
};

export default visualizestrikeRateOfBatsmen;