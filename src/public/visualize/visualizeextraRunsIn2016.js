const visualizeextraRunsIn2016 = (extraRunsIn2016) => {
    const seriesData = Object.entries(extraRunsIn2016);

    Highcharts.chart("extra_runs", {
        chart: {
            type: "column",
        },
        title: {
            text: "extra runs conceded by each team",
        },
        subtitle: {
            text:
                'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>',
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: -45,
                style: {
                    fontSize: "13px",
                    fontFamily: "Verdana, sans-serif",
                },
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: "Extras",
            },
        },
        legend: {
            enabled: false,
        },
        tooltip: {
            pointFormat: "Extras: <b>{point.y:.0f}</b>",
        },
        series: [
            {
                name: "Extras",
                data: seriesData,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: "#FFFFFF",
                    align: "center",
                    format: "{point.y:.0f}", // one decimal
                    y: 20, // 10 pixels down from the top
                    style: {
                        fontSize: "13px",
                        fontFamily: "Verdana, sans-serif",
                    },
                },
            },
        ],
    });
};

export default visualizeextraRunsIn2016;